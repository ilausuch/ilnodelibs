class ilQueue{
  constructor(){
    this.queue = []
  }

  add(element){
    this.queue.push(element)
  }

  get(){
    if (this.queue.length == 0)
      return null
    else
      return this.queue.shift()
  }

  first(){
    if (this.queue.length == 0)
      return null
    else
      return this.queue[0]
  }

  length(){
    return this.queue.length
  }

  set_consummer(callback){
    this.consumer = callback


    while (this.queue.lenght > 0 )
      callback(this.get())
  }
}

class ilQueueConsumer extends ilQueue{
  //NOTE: If you want to preserve this, the callback has to be like : (data) => { this.myCallback(data) }
  constructor(callback){
    super()
    this.callback = callback
    this.ready = false
    this.block = false
  }

  start(){
    this.ready = true
    this.release_one()
  }

  pause(){
    this.ready = false
  }

  release_one(){
    if (this.block)
      return

    if (!this.ready)
      return

    this.block = true

    setTimeout(() => {
      this.block = false

      if (this.length() > 0){
        this.ready = false
        this.callback(this.first(), ()=>{this.ack()}, this)
      }
    })
  }

  add(element){
    super.add(element)
    this.release_one()
  }

  ack(){
    this.get()
    this.ready = true
    this.release_one()
  }
}


module.exports.ilQueue = ilQueue
module.exports.ilQueueConsumer = ilQueueConsumer
