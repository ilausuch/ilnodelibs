var get_route_obj = function(route){
  if (route instanceof Route)
    return route
  else
    return new Route(route)
}

var get_route_string = function(route){
  if (route instanceof Route)
    return route.route
  else
    return route
}

module.exports.get_route_obj = get_route_obj
module.exports.get_route_string = get_route_string

class Route{
  constructor(route){
    this.route = get_route_string(route)
    this.check()
  }

  get_array(){

    return this.route.split(".")
  }

  check(){
    return true
  }

  check_match(route2){
    var route1 = this.route

    route2 = get_route_string(route2)

    if (route1 == route2 || route2 == "")
      return true

    var route1 = route1.split(".")
    var route2 = route2.split(".")

    if (route1.length != route2.length)
      return false

    var pos = 0
    for (var pos = 0; pos < route1.length; pos++){
      if (route2[pos] == "*")
        continue
      else
        if (route1[pos] != route2[pos])
          return false
    }

    return true
  }
}

class Routable {
  constructor(){
  }

  get_route(){
    throw "This method has to be implemented"
  }

  get_description(){
    throw "This method has to be implemented"
  }

  get_values(){
    return this.values
  }
}

module.exports.Route = Route
module.exports.Routable = Routable
