require('./ilEventsEmitter.min.js')
require('./ilPromises.min.js')

const ilQueueConsumer = require("./ilQueues.js").ilQueueConsumer
const Logger = require("./Logger.js").Logger

class Rabbit_connection{
  constructor(connection){
    this.connection = connection
  }
}

class Rabbit_pub_connection extends Rabbit_connection{
  constructor(connection){
    super(connection)
  }

  send(route, message){
    this.connection.publish(route, message, 'utf8');
  }
}

class Rabbit_sub_connection extends Rabbit_connection{
  constructor(connection, callback){
    super(connection)
    this.callback = callback

    this.on_message = function(data, queue){
      this.callback(data)
    }

    this.queue = new ilQueueConsumer((data) => {this.on_message(data)})
    connection.on('data', (data) => {
      var data = data.toString('utf8')
      this.queue.add(data)
    });
  }

  start(){
    this.queue.start()
  }

  ack(){
    this.queue.ack()
  }
}

class Rabbit_worker_connection extends Rabbit_sub_connection{
  constructor(connection, callback){
    super(connection, callback)

    this.on_message = function(data, queue){
      this.callback(data)
    }

    this.queue = new ilQueueConsumer((data) => {this.on_message(data)})
  }

  start(){
    this.queue.start()
  }

  ack(){
    this.queue.ack()
    this.connection.ack()
  }
}

class Rabbit_agent{
  constructor(host, user, password, auto_connect = false){
    this.host = host
    this.user = user
    this.password = password

    this.ready = false

    this.logger = global.logger || new Logger("Rabit_agent")

    this.connections = {}

    if (auto_connect)
      this.connect()
  }

  connect(fatal_on_error = true){
    var promise = new ilPromise()

    var rabbit_url = "amqp://" + this.user + ":" + this.password + "@" + this.host

    this.logger.debug("_Rabbit", "Connecting to", rabbit_url, "...")

    this.rabbitmq = require('rabbit.js').createContext(rabbit_url);

    this.rabbitmq.on('ready', () => {
      this.logger.info("_Rabbit", "Connection established")
      this.ready = true
      promise.ready()
    });

    this.rabbitmq.on('error', (error) =>{
      if (fatal_on_error)
        this.logger.fatal(error)
      else
        this.logger.error(error)
    })

    return promise
  }

  create_connexion(id, type, route, callback, options){
    var promise = new ilPromise()

    this.logger.info("_Rabbit", "Connecting to exchange/queue", id, type, route)

    var connection = this.rabbitmq.socket(type , options)

    connection.connect(route, () => {
      var connection_object = undefined
      switch(type){
        case "PUB":
          connection_object = new Rabbit_pub_connection(connection)
        break
        case "SUB":
          connection_object = new Rabbit_sub_connection(connection, callback)
        break
        case "WORKER":
          connection_object = new Rabbit_worker_connection(connection, callback)
        break
      }

      this.connections[id] = connection_object

      promise.ready(connection_object)
    })

    return promise;
  }

  create_pub(id, route, configuration = {noCreate:true}){
    return this.create_connexion(id, "PUB", route, undefined, configuration)
  }

  create_sub(id, route, callback, configuration = {noCreate:true}){
    return this.create_connexion(id, "SUB", route, callback, configuration)
  }

  create_worker(id, route, callback, configuration = {noCreate:true, prefetch: 1}){
    return this.create_connexion(id, "WORKER", route, callback, {noCreate:true, prefetch: 1})
  }

  //TODO: All kind of connections
}

module.exports.Rabbit_agent = Rabbit_agent
