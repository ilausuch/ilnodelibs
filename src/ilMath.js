module.exports.median = function(arr){
  arr = arr.sort(function(a, b){ return a - b; });
  var i = arr.length / 2;
  return i % 1 == 0 ? (arr[i - 1] + arr[i]) / 2 : arr[Math.floor(i)];
}

module.exports.wam = function(arr1, arr2){
  var sum1 = 0, sum2 = 0

  for (var i = 0; i < arr1.length; i++){
    sum1 += arr1[i] * arr2[i]
    sum2 += arr2[i]
  }

  return sum1 / sum2
}

module.exports.adv = function(arr){
  var sum = 0

  for (var i = 0; i < arr.length; i++){
    sum += arr[i]
  }

  return sum / arr.length
}

module.exports.max = function(arr){
  var max = arr[0]

  for (var i = 0; i < arr.length; i++)
    if (arr[i] > max)
      max = arr[i]

  return max
}

module.exports.min = function(arr){
  var min = arr[0]

  for (var i = 0; i < arr.length; i++)
    if (arr[i] < min)
      min = arr[i]

  return min
}

module.exports.sum = function(arr){
  var sum = 0

  for (var i = 0; i < arr.length; i++)
    sum += arr[i]

  return sum
}
