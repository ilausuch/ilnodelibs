/*
MIT license - Ivan Lausuch <ilausuch@gmail.com> 2019

This can be used when some process has to wait the end of a process

  var barrier = new Barrier()

  barrier.wait((data)=>{
    //Code after the resolution
  })

  barrier.wait((data)=>{
    //An other code after the resolution
  })

  //The process than cause the dependency will call to ready
  barrier.ready("My data")
*/

class ilBarier{
  constructor(){
    this.list = []
    this.ok = false
  }

  wait(fnc){
    this.list.push(fnc)
    if (this.ok){
      setTimeout(()=>{
        fnc(data)
      })
    }
  }

  ready(data){
    this.ok = true
    setTimeout(()=>{
      this.list.forEach((fnc) => {
        fnc(data)
      })
    })
  }
}

module.exports.ilBarier = ilBarier
