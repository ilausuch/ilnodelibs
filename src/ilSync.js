/*
MIT license - Ivan Lausuch <ilausuch@gmail.com> 2019

##Use:

### wait

It can be used As simple promise clousure

  wait((ack, error)=>{
    // Code to execute. Call to ack or error at the end
  }.then((data) => {
    // Code after the execution, the data can be captures
  })

But also you can ommit then function

  wait((ack, error)=>{
    // Code to execute. Call to ack or error at the end
  },(data) =>{
    // Code after the execution, the data can be captures
  })


### sync

Expects the number of task is going to be done and it will wait until all of them finish or
there are any error. Note: it can be used as simple promise

  sync(2, (ack, error)=>{
    //Code to execute, it should call to ack twice
  },() =>{
    // Code after the execution
  })

### wait_any

In this case you can add how much task you need and when one of then finish the promise finish

    wait_any((ack, error)=>{
      //Code to execute, it will finish when the first ack arrive
    },(data) =>{
      // Code after the execution, the data can be captures
    })

*/

var sync = function(count, toExecute, onEndCallback, onEndCallback_error){
  var promise = new Promise((resolve, reject) => {
    var done = function(data){
      count --
      if (count == 0)
        resolve(data)
    }

    var error = function(message){
      if (message instanceof Error)
        reject(message)
      else
        reject(Error(message))
    }

    try{
      toExecute(done, error)
    }catch(err){
      reject(err)
    }
  })

  if (onEndCallback !== undefined)
    promise.then(onEndCallback, onEndCallback_error)

  return promise
}

var wait = function(toExecute, onEndCallback, onEndCallback_error){
  return sync(1, toExecute, onEndCallback, onEndCallback_error)
}

var wait_any = function(toExecute, onEndCallback, onEndCallback_error){
  var resolved = false
  var promise = new Promise((resolve, reject) => {
    var done = function(data){
      if (resolved) return

      resolved = true
      resolve(data);
    }

    var error = function(message){
      if (message instanceof Error)
        reject(message)
      else
        reject(Error(message))
    }

    try{
      toExecute(done, error)
    }catch(err){
      reject(err)
    }
  })

  if (onEndCallback !== undefined)
    promise.then(onEndCallback, onEndCallback_error)

  return promise
}

module.exports.sync = sync
module.exports.wait = wait
module.exports.wait_any = wait_any
