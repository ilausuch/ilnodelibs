const get_utc_iso = require("./ilUtils.js").get_utc_iso
const colors = require('colors');

class Logger {
    constructor(name, level = "INFO"){
      this.set_level(level)

      this.name = name
      this.ready = true
    }

    set_level(level){
      switch (level) {
        case "NONE":
          this.level = 0
          break
        case "ERROR":
          this.level = 1
          break;
        case "WARNING":
          this.level = 2
          break;
        case "INFO":
          this.level = 3
          break;
        case "DEBUG":
          this.level = 4
          break;
        default:
          this.level = level
      }
    }

    log_basic(type, args){
      if (!this.ready)
        return

      var text = ""
      if (args !== undefined && args.length > 0){
        text = args.reduce((previous, current) => {
          var sep = " "
          if (previous[0]=="_"){
            sep = " - "
            previous = previous.substr(1);
          }

          return previous + sep + current;
        });

        text = "- " + text
      }
      console.info(get_utc_iso(), "-", type, text)
    }

    info(... args){
      if (this.level >= 3 )
        this.log_basic("INFO".blue, args)
    }

    error(... args){
      this.log_basic("ERROR".red, args)
    }

    fatal(... args){
      this.log_basic("FATAL".red, args)
      throw ""
      //process.exit();
    }

    warning(... args){
      if (this.level >= 2 )
        this.log_basic("WARNING".yellow, args)
    }

    debug(... args){
      if (this.level >= 4 )
        this.log_basic("DEBUG".magenta, args)
    }

    raw(... args){
      if (this.level < 4) return

      if (args !== undefined && args.length > 0){
        var list = []
        for(var i=0; i<args.length; i++){
          if (typeof args[i] == "string" || typeof args[i] == "Number")
            list.push(args[i])
          else
            break
        }
        this.log_basic("DEBUG".magenta, list)

        for(; i<args.length; i++)
          console.log(args[i])
      }
    }

    json(... args){
      if (this.level < 4) return

      for(var i=0; i<args.length; i++){
        if (typeof args[i] != "string" && typeof args[i] != "Number")
          args[i] = JSON.stringify(args[i])
      }

      this.log_basic("DEBUG".magenta, args)
    }

    pause(){
      this.ready = false
    }

    resume(){
      this.ready = true
    }
}

module.exports.Logger = Logger
