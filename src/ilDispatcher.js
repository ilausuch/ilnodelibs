const libs_path = "./"

require(libs_path + 'ilEventsEmitter.min.js')
require(libs_path + 'ilPromises.min.js')

const ilQueueConsumer = require(libs_path + "ilQueues.js").ilQueueConsumer
const Routes = require(libs_path + "Routes.js")

class ilDispatcher {
  constructor(){
    this.input = new ilQueueConsumer((data, ack, queue) => {
      this.dispatch_message(data, ack)
    })
    this.registry = []
  }

  connect_queue(route, queue){
    this.registry.push({route:route, queue:queue})
  }

  start(){
    this.input.start()
  }

  pause(){
    this.input.pause()
  }

  send(route, message){
    this.input.add({route:route, message:message})
  }

  dispatch_message(data, ack){
    this.registry.forEach((reg) =>{
      if (Routes.get_route_obj(data.route).check_match(reg.route))
        reg.queue.add(data)
    })

    this.input.ack()
  }
}


module.exports.ilDispatcher = ilDispatcher
